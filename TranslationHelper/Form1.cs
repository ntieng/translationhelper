﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Windows.Forms;
namespace TranslationHelper
{
    public partial class Form1 : Form
    {
        string[] files = null;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            //this.openFileDialog = new OpenFileDialog();
            //using (var opnDlg = new OpenFileDialog())
            //{
            //    if (opnDlg.ShowDialog() == DialogResult.OK)
            //    {
            //        textBox1.Text = Path.GetDirectoryName(opnDlg.FileName);
            //    }
            //}

            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    files = Directory.GetFiles(fbd.SelectedPath);
                    string[] fileName = new string[3];
                    bool enFileExist = false;
                    bool zhFileExist = false;
                    bool msFileExist = false;
                    foreach (var file in files)
                    {
                        switch (Path.GetFileName(file))
                        {
                            case "en.json":
                                fileName[0] = Path.GetFileName(file);
                                enFileExist = true;
                                break;
                            case "zh.json":
                                fileName[1] = Path.GetFileName(file);
                                zhFileExist = true;
                                break;
                            case "ms.json":
                                fileName[2] = Path.GetFileName(file);
                                msFileExist = true;
                                break;
                            default:
                                break;
                        }
                    }
                    if (enFileExist && zhFileExist && msFileExist)
                    {
                        textBox1.PlaceholderText = fbd.SelectedPath;
                        dataGridView1.Show();
                        btnAddToFile.Show();
                        richTextBox2.Show();
                        btnResetBox.Show();
                    }
                    else
                    {
                        dataGridView1.Hide();
                        btnAddToFile.Hide();
                        richTextBox2.Hide();
                        btnResetBox.Hide();
                        MessageBox.Show("En.json,Zh.json or Ms.json not found ", "Message");
                    }
                }
            }
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (dataGridView1.SelectedCells.Count > 0)
                dataGridView1.ContextMenuStrip = contextMenuStrip1;
        }

        private void PasteClipboard()
        {
            try
            {
                dataGridView1.Rows.Add();
                string s = Clipboard.GetText();
                string[] lines = s.Replace("\n", "").Split('\r');
                string[] fields;
                int row = 0;
                int column = 0;
                foreach (string l in lines)
                {
                    fields = l.Split('\t');
                    foreach (string f in fields)
                    {
                        dataGridView1[column++, row].Value = f;
                    }
                    dataGridView1.Rows.Add();
                    row++;
                    column = 0;
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("The data you pasted is in the wrong format for the cell");
                return;
            }
        }

        private void contextMenuStrip1_Click(object sender, EventArgs e)
        {
            PasteClipboard();
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
        }

        private void btnAddToFile_Click(object sender, EventArgs e)
        {
            dataGridView1.Enabled = false;
            btnAddToFile.Enabled = false;
            bool hasError = false;
            try
            {
                //This for loop loops through each row in the table
                for (int r = 0; r <= dataGridView1.Rows.Count - 1; r++)
                {
                    //This for loop loops through each column, and the row number
                    //is passed from the for loop above.

                    if (dataGridView1.Columns.Count == 4 &&
                        dataGridView1.Rows[r].Cells[0].Value != null &&
                        dataGridView1.Rows[r].Cells[1].Value != null &&
                        dataGridView1.Rows[r].Cells[2].Value != null &&
                        dataGridView1.Rows[r].Cells[3].Value != null)
                    {
                        var jsonKey = dataGridView1.Rows[r].Cells[0].Value.ToString();
                        var jsonValue = "";
                        foreach (var file in files)
                        {
                            var json = File.ReadAllText(file);
                            bool isEnglishFileExist = false;
                            bool isChineseFileExist = false;
                            bool isMalayFileExist = false;
                            // check user got en zh and ms file or not
                            switch (Path.GetFileName(file))
                            {
                                case "en.json":
                                    isEnglishFileExist = true;
                                    jsonValue = dataGridView1.Rows[r].Cells[1].Value.ToString();
                                    break;
                                case "zh.json":
                                    isChineseFileExist = true;
                                    jsonValue = dataGridView1.Rows[r].Cells[2].Value.ToString();
                                    break;
                                case "ms.json":
                                    isMalayFileExist = true;
                                    jsonValue = dataGridView1.Rows[r].Cells[3].Value.ToString();

                                    break;
                                default:
                                    break;
                            }
                            if (isMalayFileExist || isChineseFileExist || isEnglishFileExist)
                            {
                                try
                                {
                                    var jObject = JObject.Parse(json);
                                    if (jObject != null)
                                    {
                                        jObject.Add(jsonKey, jsonValue);
                                        string newJsonResult = JsonConvert.SerializeObject(jObject, Formatting.Indented);
                                        File.WriteAllText(file, newJsonResult);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    hasError = true;
                                    MessageBox.Show("Error Message : " + ex.Message.ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception err)
            {
                hasError = true;
                MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (!hasError)
            {
                MessageBox.Show("Insert Complete.", "Program Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                printKeyAtTextBox();
            }
        }

        public void printKeyAtTextBox()
        {
            try
            {
                //This for loop loops through each row in the table
                for (int r = 0; r <= dataGridView1.Rows.Count - 1; r++)
                {
                    // because only want get the json key so only get he first cell
                    if (dataGridView1.Rows[r].Cells[0].Value != null)
                    {
                        if (string.IsNullOrEmpty(dataGridView1.Rows[r].Cells[0].Value.ToString()))
                        {
                            continue;
                        }
                        string jsonKey = dataGridView1.Rows[r].Cells[0].Value.ToString();
                        richTextBox2.AppendText("{{'" + jsonKey + "'|translate}}" + "\r\n");
                    }

                }

                //MessageBox.Show("Print Complete.", "Program Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (System.Exception err)
            {
                MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btnResetBox_Click(object sender, EventArgs e)
        {
            //Clear Data Grid View and enabled it
            dataGridView1.Enabled = true;
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();

            btnAddToFile.Enabled = true;
            richTextBox2.Clear();
            //richTextBox3.Clear();
        }
    }
}
